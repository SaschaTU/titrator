/*
 * Stepper.h - Stepper library for Wiring/Arduino - Version 1.1.0
 *
 * Original library        (0.1)   by Tom Igoe.
 * Two-wire modifications  (0.2)   by Sebastian Gassner
 * Combination version     (0.3)   by Tom Igoe and David Mellis
 * Bug fix for four-wire   (0.4)   by Tom Igoe, bug fix from Noah Shibley
 * High-speed stepping mod         by Eugene Kozlenko
 * Timer rollover fix              by Eugene Kozlenko
 * Five phase five wire    (1.1.0) by Ryan Orendorff
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef Stepper_h
#define Stepper_h

class Stepper {
private:
    void stepMotor(char curSeg);
    bool direction;
    unsigned long usPerStep;
    unsigned int stepsPerRevolution;
    long curStep;
    char pin1;
    char pin2;
    char pin3;
    char pin4;
    unsigned long lastStepUs;

public:
    Stepper(int spr, int rpm,
        char outPin1, char outPin2,
        char outPin3, char outPin4);
    void setDirection(bool dir) {direction = dir;}
    bool getDirection() {return direction;}
    long getCurStep() {return curStep;}
    void step(int nSteps);
};

#endif

