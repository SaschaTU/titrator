/*
 * Stepper.cpp - Stepper library for Wiring/Arduino - Version 1.1.0
 *
 * Original library        (0.1)   by Tom Igoe.
 * Two-wire modifications  (0.2)   by Sebastian Gassner
 * Combination version     (0.3)   by Tom Igoe and David Mellis
 * Bug fix for four-wire   (0.4)   by Tom Igoe, bug fix from Noah Shibley
 * High-speed stepping mod         by Eugene Kozlenko
 * Timer rollover fix              by Eugene Kozlenko
 * Five phase five wire    (1.1.0) by Ryan Orendorff
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "Arduino.h"
#include "Stepper.h"

Stepper::Stepper(int spr, int rpm,
    char outPin1, char outPin2,
    char outPin3, char outPin4) {

    curStep = 0; // which step the motor is on
    direction = 0; // motor direction
    lastStepUs = 0; // time stamp in us of the last step taken
    stepsPerRevolution = spr; // steps per revolution
    usPerStep = 60000000 / rpm / stepsPerRevolution; // time per step

    pin1 = outPin1;
    pin2 = outPin2;
    pin3 = outPin3;
    pin4 = outPin4;

    pinMode(pin1, OUTPUT);
    pinMode(pin2, OUTPUT);
    pinMode(pin3, OUTPUT);
    pinMode(pin4, OUTPUT);
}


/*
 * Moves the motor nSteps.  If the number is negative,
 * the motor moves in the reverse direction.
 */
void Stepper::step(int nSteps) {
    unsigned int leftSteps = abs(nSteps);
    direction = (nSteps > 0 ? 1 : 0);

    while (leftSteps > 0) {
        unsigned long now = micros();
        if (now - lastStepUs >= usPerStep) {
            lastStepUs = now;
            curStep += (direction ? 1 : -1);
            --leftSteps;
            stepMotor(curStep % 4);
        }
    }
}

/*
 * Moves the motor forward or backwards.
 */
void Stepper::stepMotor(char curSeg) {
    switch (curSeg) {
    case 0: // 1010
        digitalWrite(pin1, HIGH);
        digitalWrite(pin2, LOW);
        digitalWrite(pin3, HIGH);
        digitalWrite(pin4, LOW);
        break;
    case 1: // 0110
        digitalWrite(pin1, LOW);
        digitalWrite(pin2, HIGH);
        digitalWrite(pin3, HIGH);
        digitalWrite(pin4, LOW);
        break;
    case 2: //0101
        digitalWrite(pin1, LOW);
        digitalWrite(pin2, HIGH);
        digitalWrite(pin3, LOW);
        digitalWrite(pin4, HIGH);
        break;
    case 3: //1001
        digitalWrite(pin1, HIGH);
        digitalWrite(pin2, LOW);
        digitalWrite(pin3, LOW);
        digitalWrite(pin4, HIGH);
        break;
    }
}
