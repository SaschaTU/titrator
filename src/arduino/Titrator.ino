#include <StaticThreadController.h>
#include <Stepper.h>

// ---------------------------------------------------------
// Global variables and constants
// ---------------------------------------------------------

const byte pinValveE = 9; // Valve (Enable)
const byte pinValve1 = 4; // Valve (In 1)
const byte pinValve2 = 5; // Valve (In 2)

// const byte pinEncoder = 2; // Encoder (In)

const byte pinStepperAp = 6; // Stepper (In A+)
const byte pinStepperAm = 7; // Stepper (In A-)
const byte pinStepperBp = 10; // Stepper (In B+)
const byte pinStepperBm = 11; // Stepper (In B-)

// const byte pinMotorE = 3; // DC motor (Enable)
// const byte pinMotor1 = 6; // DC motor (In 1)
// const byte pinMotor2 = 7; // DC motor (In 2)

long posMotor = 0; // Current motor position (steps)
unsigned int pH = 0; // pH electrode data (0-1024)
bool dirMotor = 0; // CW -> 0, CCW -> 1
bool onMotor = 0; // Is the motor working?
bool onValve = 0; // Is the valve open?

Stepper stepper(200, 120,
    pinStepperAp, pinStepperAm,
    pinStepperBp, pinStepperBm);

Thread threadPH(&pollPh, 10);
Thread threadSerial(&communicateSerial, 10);
Thread threadMotor(&driveMotorValve, 10);
// Thread threadEncoder(&pollEncoder, 5);

StaticThreadController<3> controlThread(
    &threadPH,
    &threadSerial,
    &threadMotor);

// ---------------------------------------------------------
// Functions
// ---------------------------------------------------------

/// Ensures serial communication between Arduino and PC
void communicateSerial() {
    // Serial to PC
    byte sendArray[6] = {
        (byte)(posMotor >> 24),
        (byte)((posMotor >> 16) & 0xFF),
        (byte)((posMotor >> 8) & 0xFF),
        (byte)(posMotor & 0xFF),
        (byte)(pH >> 2),
        (byte)((pH & 0x3 << 6) | (onMotor << 5) |
            (dirMotor << 4) | (onValve << 3))
    };
    Serial.write(sendArray, 6);

    // Serial from PC
    if (Serial.available() > 0) {
        byte inByte = Serial.read();
        onMotor = inByte >> 7;
        dirMotor = (inByte >> 6) & 0x01;
        onValve = (inByte >> 5) & 0x01;
    }
}

/// Controls the motor and the valve
void driveMotorValve() {
    digitalWrite(pinValveE, onValve);
    if (onMotor) {
        int st = dirMotor ? 2 : -2;
        stepper.step(st);
        posMotor += st;
    }
}

/// Reads the input from the pH electrode
void pollPh() {
    static int aggrPh = 0;
    static byte nRead = 0;
    aggrPh += analogRead(0);
    if (++nRead == 10) {
        pH = aggrPh / 10;
        nRead = 0;
        aggrPh = 0;
    }
}

/*
/// Updates the current motor axis position
void pollEncoder() {
    bool encoderCurrent = digitalRead(pinEncoder);
    static bool encoderLast = encoderCurrent;
    if (encoderLast == 0 && encoderCurrent == 1) {
        if (dirMotor)
            ++posMotor;
        else
            --posMotor;
    }
    encoderLast = encoderCurrent;
}
*/

// ---------------------------------------------------------
// Setup and main loop
// ---------------------------------------------------------

void setup() {
    pinMode(pinValveE, OUTPUT);
    pinMode(pinValve1, OUTPUT);
    pinMode(pinValve2, OUTPUT);

    digitalWrite(pinValveE, 0);
    digitalWrite(pinValve1, 1);
    digitalWrite(pinValve2, 0);

    Serial.begin(57600);
}

void loop() {
    controlThread.run();
}
