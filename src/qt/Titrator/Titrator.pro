QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Titrator
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp \
           titrator.cpp

HEADERS += titrator.h

FORMS += titrator.ui
