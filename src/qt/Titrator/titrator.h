#ifndef TITRATOR_H
#define TITRATOR_H

#include <QWidget>

namespace Ui {
    class Titrator;
}

class Titrator : public QWidget {
    Q_OBJECT

public:
    explicit Titrator(QWidget *parent = 0);
    ~Titrator();

private:
    Ui::Titrator *ui;
};

#endif // TITRATOR_H
